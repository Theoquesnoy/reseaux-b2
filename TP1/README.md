QUESNOY Théo

# TP1 - Mise en jambes

## I. Exploration en solo

### 1. Affichage d'informations sur la pile TCP/IP locale

* Infos des cartes réseaux

Les informations des cartes réseaux se fait avec la commande `ipconfig -all`

Interface réseau WIFI : 
```
Adresse IPv4. . . . . . . . . . . . . .: 10.33.1.24
Description. . . . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
Adresse physique . . . . . . . . . . . : 08-71-90-C7-C7-7A
```

Interface réseau Ethernet :
```
Statut du média. . . . . . . . . . . . : Média déconnecté
Adresse physique . . . . . . . . . . . : 98-FA-9B-FC-F9-B8
Description. . . . . . . . . . . . . . : Realtek PCIe GbE Family Controller
```

Passerelle de la carte WIFI : 
`Passerelle par défaut. . . . . . . . . : 10.33.3.253`

Interface GUI :

![image alt](../images/TP1/Infos_carte_WIFI_GUI.PNG)


* La passerelle dans le réseau d'YNOV sert à communiquer avec d'autres réseaux extérieurs

### 2. Modifications des informations

#### A. Modification d'adresse IP (part 1)

Changement de l'adresse IP manuellement :

![image alt](../images/TP1/IP_changé_GUI.PNG)

* L'accès à internet peut être perdu car on peut perdre l'accès à la passerelle du réseau si l'adresse IP changé est mal paramétrer ou écrite.

#### B. Table ARP

Liste ARP : 
```
C:\Users\theoq>arp -a
[...]
Interface : 10.33.1.246 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.33.2.173           34-2e-b7-47-f9-28     dynamique
  10.33.2.190           74-4c-a1-d8-e6-03     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```

* Adresse MAC de la passerelle du réseau : `10.33.3.253           00-12-00-40-4c-bf     dynamique`

J'ai remarqué cette adresse MAC car elle était répertorié dans l'interfarce 10.33.1.246 qui est l'adresse IP que j'ai modifié manuellement à l'étape précédente

Ping d'adresses IP aléatoires : `ping 10.33.3.112`, `ping 10.33.3.130`, `ping 10.33.3.125`

La liste ARP s'est donc rallongé avec les nouvelles adresses IP : 

```
C:\Users\theoq>arp -a
[...]
10.33.2.190           74-4c-a1-d8-e6-03     dynamique
10.33.3.109           38-f9-d3-2f-c2-79     dynamique
10.33.3.112           3c-06-30-2d-48-0d     dynamique
10.33.3.125           e0-d4-64-f4-25-7c     dynamique
10.33.3.253           00-12-00-40-4c-bf     dynamique
[...]
```

#### C. nmap

![image alt](../images/TP1/Resultat_ping_nmap.PNG)

La liste ARP s'est de nouveau rallongé : 

```
C:\Users\theoq>arp -a
[...]
Interface : 10.33.1.247 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.33.0.27            a8-64-f1-8b-1d-4d     dynamique
  10.33.0.45            f2-4a-90-92-79-12     dynamique
  10.33.0.164           9a-75-ee-ef-e2-00     dynamique
  10.33.0.210           70-66-55-47-54-15     dynamique
  10.33.1.8             0c-dd-24-aa-e1-87     dynamique
  10.33.1.28            78-d7-5f-c8-5a-81     dynamique
  10.33.1.172           50-e0-85-db-61-8b     dynamique
  10.33.1.236           92-27-3b-ce-ae-1b     dynamique
  10.33.2.209           5c-87-9c-e4-44-2c     dynamique
  10.33.2.237           be-fd-16-82-f4-05     dynamique
  10.33.3.13            26-7b-3f-46-6d-9e     dynamique
  10.33.3.15            26-09-60-6e-98-26     dynamique
  10.33.3.23            7e-7e-4e-bb-23-88     dynamique
  10.33.3.24            ac-12-03-2e-e4-92     dynamique
  10.33.3.40            0e-66-80-86-94-d6     dynamique
  10.33.3.41            3c-58-c2-14-aa-5a     dynamique
  10.33.3.65            48-e7-da-69-32-77     dynamique
  10.33.3.168           12-88-c1-71-b7-a0     dynamique
  10.33.3.207           c0-e4-34-1b-fe-a9     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
```

#### D. Modification d'adresse IP (part 2)

En fonction du résultat de nmap, j'ai donc choisi une IP qui était libre et en mettant la passerelle d'ynov : 

![image alt](../images/TP1/IP_choisi_libre_nmap.PNG)

J'ai donc vérifié les modifications de l'adresse IP et de la passerelle avec la commande `ipconfig` et vérifié également l'accès à internet avec `ping 8.8.8.8`: 
```
C:\Users\theoq>ipconfig

Configuration IP de Windows
[...]
Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::b1ae:d610:c4fe:a451%7
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.3.220
   Masque de sous-réseau. . . . . . . . . : 255.255.255.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253

C:\Users\theoq>ping 8.8.8.8

Envoi d’une requête 'Ping'  8.8.8.8 avec 32 octets de données :
Réponse de 8.8.8.8 : octets=32 temps=46 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=20 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=18 ms TTL=115
Réponse de 8.8.8.8 : octets=32 temps=19 ms TTL=115

Statistiques Ping pour 8.8.8.8:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 18ms, Maximum = 46ms, Moyenne = 25ms
```

## II Exploration en duo (avec Yann Farant)

### 3. Modification d'adresse IP

Changement d'adresse IP + ping en connexion Ethernet : 
```
C:\Users\theoq>ipconfig

Configuration IP de Windows


Carte Ethernet Ethernet :

   Suffixe DNS propre à la connexion. . . :
   Adresse IPv6 de liaison locale. . . . .: fe80::c93e:228a:f158:304c%23
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.10.1
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
   Passerelle par défaut. . . . . . . . . :
   
C:\Users\theoq>ping 192.168.10.2

Envoi d’une requête 'Ping'  192.168.10.2 avec 32 octets de données :
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.10.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
```

Table ARP du réseau Ethernet :
```
C:\Users\theoq>arp -a
[...]
Interface : 192.168.10.1 --- 0x17
  Adresse Internet      Adresse physique      Type
  192.168.10.2          e8-6a-64-39-cf-32     dynamique
  192.168.10.3          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
[...]
```

### 4. Utilisation d'un des deux comme gateway

Partage de connexion de la carte WIFI avec ethernet : 

![image alt](../images/TP1/Carte_wifi_partagé.PNG)
![image alt](../images/TP1/Connexion_partagé_avec_Ethernet.PNG)

La connexion internet via ethernet marche car le ping 8.8.8.8 fonctionne
![image alt](../images/TP1/Internet_via_Ethernet+Passerelle.png)

Et on voit bien avec la commande `tracert` que la connexion passe bien par mon ordinateur 
![image alt](../images/TP1/Tracert_ping_8.8.8.8.png)

### 5. Petit chat privé

Chat netcat crée : 
```
PS C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11> .\nc.exe -l -p 6868
coucou !
salut
C'est moi !
Etape 5 accomplie
Etape réussi par Théo !
ET yann!
```
Par adresse IP aussi : 
```
PS C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11> .\nc.exe -l -p 6868 192.168.10.2
ca marche ?
oui mon capitaine
```

Ping réussi avec pare feu : 

![image alt](../images/TP1/Règles_firewall_ping+netcat.PNG)
```
PS C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11> ping 192.168.10.2

Envoi d’une requête 'Ping'  192.168.10.2 avec 32 octets de données :
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128
Réponse de 192.168.10.2 : octets=32 temps<1ms TTL=128

Statistiques Ping pour 192.168.10.2:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
PS C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11>
```

Netcat marche avec pare feu : 
```
PS C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11> .\nc.exe -l -p 6565
Alors ca marche avec le pare feu ?
Niquel sergent
```
## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

L'adresse IP du serveur DHCP d'YNOV et le bail obtenu/expirant se trouve avec la commande `ipconfig /all`

```
C:\Users\theoq>ipconfig /all

Configuration IP de Windows
[...]
   Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 14:14:43
   Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 16:14:43
[...]
   Serveur DHCP . . . . . . . . . . . . . : 10.33.3.254
```

### 2. DNS

Les adresses IP DNS sont plusieurs à être connues par mon ordinateur et elle se trouve avec `ipconfig /all`
```
C:\Users\theoq>ipconfig /all

Configuration IP de Windows
[...]
   Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                       10.33.10.148
                                       10.33.10.155
[...]
```

Résultat des commandes `nslookup` de google et d'ynov : 
```
C:\Users\theoq>nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:813::200e
          216.58.214.174


C:\Users\theoq>nslookup ynov.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    ynov.com
Address:  92.243.16.143
```
L'adresse 10.33.10.2 correspond au DNS du réseau d'ynov.

On fait une recherche par le nom d'un domaine, dans ce cas c'est celui de google et d'ynov, et la commande va donx nous dire quel adresse IP correspond à ce nom de domaine

La réponse ne faisant pas autorité veut dire que le serveur qui nous répond n'a pas de pouvoir sur le domaine.

Résultat des commandes inversées pour `nslookup` : 
```
C:\Users\theoq>nslookup 78.74.21.21
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21


C:\Users\theoq>nslookup 92.146.54.88
Serveur :   UnKnown
Address:  10.33.10.2

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
L'adresse 10.33.10.2 correspond au DNS du réseau d'ynov.

Avec comme recherche l'adresse IP, la commande va nous donner quel nom correspond à tel IP. Dans ce cas-là, l'ip 92.146.54.88 correspond au nom 'apoitiers-654.....'

La réponse ne faisant pas autorité veut dire que le serveur qui nous répond n'a pas de pouvoir sur le domaine

## IV. Wireshark

Ping vers la passerelle : 
```
C:\Users\theoq>ping 10.33.3.253

Envoi d’une requête 'Ping'  10.33.3.253 avec 32 octets de données :
Réponse de 10.33.3.253 : octets=32 temps=4 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=3 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=5 ms TTL=255
Réponse de 10.33.3.253 : octets=32 temps=4 ms TTL=255

Statistiques Ping pour 10.33.3.253:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 3ms, Maximum = 5ms, Moyenne = 4ms
```
![image alt](../images/TP1/Ping_vers_passerelle.PNG)

Conversation netcat : 
```
C:\Users\theoq\Desktop\Ynov\Deuxième Année\netcat-1.11>nc.exe -l -p 6565
coucou
ca marche avec moi meme
```

![image alt](../images/TP1/Wireshark_netcat.PNG)

Requête DNS de google : 
```
C:\Users\theoq>nslookup google.com
Serveur :   UnKnown
Address:  10.33.10.2

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:80c::200e
          216.58.201.238
```

![image alt](../images/TP1/Requete_DNS.PNG)

