QUESNOY Théo

# TP3 : Progressons vers le réseau d'infrastructure

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie-tcp-et-udp)
- [V. El final](#v-el-final)

## I. (mini)Architecture réseau

### 2. Routeur

Adresses IP du routeur.tp3 : 
```
[theo@routeur ~]$ ip a
[...]
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:76:10:a0 brd ff:ff:ff:ff:ff:ff
    inet 10.3.50.62/26 brd 10.3.50.63 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe76:10a0/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ec:be:b3 brd ff:ff:ff:ff:ff:ff
    inet 10.3.51.126/25 brd 10.3.51.127 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feec:beb3/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:16:b2:b5 brd ff:ff:ff:ff:ff:ff
    inet 10.3.52.14/28 brd 10.3.52.15 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe16:b2b5/64 scope link
       valid_lft forever preferred_lft forever
```

Accès internet : 

```
[theo@routeur ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=22.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=22.3 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 22.262/22.285/22.308/0.023 ms
```
Résolution de nom : 

```
[theo@routeur ~]$ ping -c 2 google.com
PING google.com (216.58.214.78) 56(84) bytes of data.
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=1 ttl=114 time=25.2 ms
64 bytes from fra15s10-in-f78.1e100.net (216.58.214.78): icmp_seq=2 ttl=114 time=23.4 ms

--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 23.449/24.333/25.217/0.884 ms
```

Activation routage : 

```
[theo@routeur ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
[theo@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[theo@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

Hostname : 
```
[theo@routeur ~]$ sudo hostname routeur.tp3
[theo@routeur ~]$ echo 'routeur.tp3' | sudo tee /etc/hostname
[theo@routeur ~]$ hostname
routeur.tp3
```

# II. Services d'infra

## 1. Serveur DHCP

* VM dhcp.client1.tp3

Hostname :
```
[theo@dhcp ~]$ sudo hostname dhcp.client1.tp3
[theo@dhcp ~]$ echo 'dhcp.client1.tp3' | sudo tee /etc/hostname
[theo@dhcp ~]$ hostname
dhcp.client1.tp3
```
dhcpd.conf : 

```
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers      1.1.1.1;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.3.50.0 netmask 255.255.255.192 {
    # specify the range of lease IP address
    range dynamic-bootp 10.3.50.2 10.3.50.60;
    # specify broadcast address
    option broadcast-address 10.3.50.63;
    # specify gateway
    option routers 10.3.50.62;
}
```
Fichier [dhcpd.conf](./dhcpd.conf)

Activation serveur DHCP : 

```
[theo@dhcp ~]$ sudo systemctl enable --now dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
```

On peut voir que marcel a pris une ip du serveur DHCP : 

```
[theo@dhcp ~]$ cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

lease 10.3.50.131 {
  starts 4 2021/09/30 14:35:40;
  ends 4 2021/09/30 14:45:40;
  tstp 4 2021/09/30 14:45:40;
  cltt 4 2021/09/30 14:35:40;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:53:e3:88;
  uid "\001\010\000'S\343\210";
  client-hostname "marcel";
}
server-duid "\000\001\000\001(\350\206\216\010\000'c\344\261";
```

* VM marcel

VM marcel : 

```
[theo@localhost ~]$ sudo hostname marcel.client1.tp3
[sudo] password for theo:
[theo@localhost ~]$ echo 'marcel.client1.tp3' | sudo tee /etc/hostname
marcel.client1.tp3
[theo@localhost ~]$ sudo hostname
marcel.client1.tp3
```

Adresse IP via DHCP serveur : 
```
[theo@marcel ~]$ sudo dhclient enp0s8
[theo@marcel ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:53:e3:88 brd ff:ff:ff:ff:ff:ff
    inet 10.3.50.131/26 brd 10.3.50.191 scope global dynamic enp0s8
       valid_lft 600sec preferred_lft 600sec
    inet6 fe80::a00:27ff:fe53:e388/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Accès internet : 
```
[theo@marcel ~]$ ping -c 2 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=20.5 ms

--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 19.349/19.904/20.460/0.573 ms
```

DNS : 
```
[theo@marcel ~]$ ping -c 2 ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=52 time=19.6 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=52 time=20.2 ms

--- ynov.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 5088ms
rtt min/avg/max/mdev = 19.578/19.876/20.175/0.330 ms
```

Traceroute : 
```
[theo@marcel ~]$ traceroute ynov.com
traceroute to ynov.com (92.243.16.143), 30 hops max, 60 byte packets
 1  _gateway (10.3.50.190)  1.992 ms  1.907 ms  1.720 ms
 2  10.0.2.2 (10.0.2.2)  1.877 ms  1.746 ms  1.546 ms
 3  * * *
 [...]
```

## 2. Serveur DNS

### B. SETUP copain

check dns file : 
```
[theo@dns1 ~]$ sudo named-checkconf
[theo@dns1 ~]$ sudo named-checkzone server1.tp3 /var/named/server1.tp3.forward
zone server1.tp3/IN: loaded serial 2021062301
OK
```

firewall : 
```
[theo@dns1 ~]$ sudo firewall-cmd --add-service=dns
success
[theo@dns1 ~]$ sudo firewall-cmd --add-service=dns --permanent
success
[theo@dns1 ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: dhcpv6-client dns ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

Démarrage serveur dns : 
```
[theo@dns1 ~]$ sudo named-checkconf
[theo@dns1 ~]$ sudo named-checkzone server1.tp3 /var/named/server1.tp3.forward
zone server1.tp3/IN: loaded serial 2021062301
OK
[theo@dns1 ~]$ systemctl enable --now named
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ====
Authentication is required to manage system service or unit files.
Authenticating as: theo
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ====
Authentication is required to reload the systemd state.
Authenticating as: theo
Password:
==== AUTHENTICATION COMPLETE ====
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-units ====
Authentication is required to start 'named.service'.
Authenticating as: theo
Password:
==== AUTHENTICATION COMPLETE ====
[theo@dns1 ~]$ systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
   Loaded: loaded (/usr/lib/systemd/system/named.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2021-10-04 14:31:29 CEST; 50s ago
[...]
```

## 3. Get deeper

### A. DNS forwarder
Vérification avec dig : 
```
[theo@marcel ~]$ dig google.fr @10.3.50.125

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.fr @10.3.50.125
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 34052
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 4, ADDITIONAL: 9

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: 4bfddd84941da7363d8da5aa615af79bdce24c62e44be4a9 (good)
;; QUESTION SECTION:
;google.fr.                     IN      A

;; ANSWER SECTION:
google.fr.              196     IN      A       142.250.178.131

;; AUTHORITY SECTION:
google.fr.              172696  IN      NS      ns2.google.com.
google.fr.              172696  IN      NS      ns1.google.com.
google.fr.              172696  IN      NS      ns3.google.com.
google.fr.              172696  IN      NS      ns4.google.com.

;; ADDITIONAL SECTION:
ns4.google.com.         172696  IN      A       216.239.38.10
ns2.google.com.         172696  IN      A       216.239.34.10
ns1.google.com.         172696  IN      A       216.239.32.10
ns3.google.com.         172696  IN      A       216.239.36.10
ns4.google.com.         172696  IN      AAAA    2001:4860:4802:38::a
ns2.google.com.         172696  IN      AAAA    2001:4860:4802:34::a
ns1.google.com.         172696  IN      AAAA    2001:4860:4802:32::a
ns3.google.com.         172696  IN      AAAA    2001:4860:4802:36::a

;; Query time: 1 msec
;; SERVER: 10.3.50.125#53(10.3.50.125)
;; WHEN: Mon Oct 04 14:46:19 CEST 2021
;; MSG SIZE  rcvd: 340
```

C'est bien mon serveur dns qui répond : 
`;; SERVER: 10.3.50.125#53(10.3.50.125)`

### B. On revient sur la conf du DHCP

Changment de DNS de 1.1.1.1 avec l'adresse IP de mon serveur DNS : 

```
[theo@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf | grep '10.3.50.125'
option domain-name-servers     10.3.50.125;
```

On voit que johnny a récupérer une adresse IP : 
```
[theo@dhcp ~]$ cat /var/lib/dhcpd/dhcpd.leases
[...]
lease 10.3.50.132 {
  starts 1 2021/10/04 14:20:32;
  ends 1 2021/10/04 14:30:32;
  cltt 1 2021/10/04 14:20:32;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:8c:72:4e;
  uid "\001\010\000'\214rN";
  client-hostname "johnny";
[...]
}
```

Commande `ip a` sur la VM johnny : 

```
[theo@johnny ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8c:72:4e brd ff:ff:ff:ff:ff:ff
    inet 10.3.50.132/26 brd 10.3.50.191 scope global dynamic noprefixroute enp0s8
       valid_lft 565sec preferred_lft 565sec
    inet6 fe80::a00:27ff:fe8c:724e/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Vérification internet + DNS avec ping :
```
[theo@johnny ~]$ ping -c 3 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=117 time=32.9 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=117 time=32.10 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=117 time=34.3 ms

--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2002ms
rtt min/avg/max/mdev = 32.947/33.401/34.260/0.607 ms
[theo@johnny ~]$ ping -c 3 google.com
PING google.com (216.58.214.174) 56(84) bytes of data.
64 bytes from mad01s26-in-f174.1e100.net (216.58.214.174): icmp_seq=1 ttl=117 time=48.1 ms
64 bytes from mad01s26-in-f174.1e100.net (216.58.214.174): icmp_seq=2 ttl=117 time=34.2 ms
64 bytes from mad01s26-in-f174.1e100.net (216.58.214.174): icmp_seq=3 ttl=117 time=34.5 ms

--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 6058ms
rtt min/avg/max/mdev = 34.207/38.939/48.062/6.454 ms
```

Vérification avec dig : 
```
[theo@johnny ~]$ dig ynov.com
[...]
;; QUESTION SECTION:
;ynov.com.                      IN      A

;; ANSWER SECTION:
ynov.com.               10800   IN      A       92.243.16.143

;; AUTHORITY SECTION:
ynov.com.               172800  IN      NS      ns-78-c.gandi.net.
ynov.com.               172800  IN      NS      ns-111-b.gandi.net.
ynov.com.               172800  IN      NS      ns-177-a.gandi.net.

;; Query time: 608 msec
;; SERVER: 10.3.50.125#53(10.3.50.125)
;; WHEN: Tue Oct 05 16:40:51 CEST 2021
;; MSG SIZE  rcvd: 158
```


# III. Services métier

## 1. Serveur Web

Création page html avec httpd sur web-server2

Vérification d'accès depuis marcel-client1

```
[theo@marcel ~]$ curl 10.3.50.205
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
[...]
```

## 2. Partage de fichiers

### B. Le setup wola

* Création du serveur nfs 
```
[theo@nfs1 ~]$ sudo dnf -y install nfs-utils
[...]
[theo@nfs1 ~]$ sudo vi /etc/idmapd.conf
[theo@nfs1 ~]$ sudo vim /etc/exports
[theo@nfs1 ~]$ sudo mkdir /srv/nfs_share
[theo@nfs1 ~]$ systemctl enable --now rpcbind nfs-server
[theo@nfs1 ~]$ sudo firewall-cmd --add-service=nfs
success
[theo@nfs1 ~]$ sudo firewall-cmd --add-service=nfs --permanent
success
[theo@nfs1 ~]$ sudo exportfs -a
[...]
```

* Configuration côté client

```
[theo@web1 ~]$ sudo dnf install -y nfs-utils
[theo@web1 ~]$ sudo vim /etc/idmapd.conf
[theo@web1 ~]$ sudo mkdir /srv/nfs
[theo@web1 ~]$ sudo mount -t nfs nfs1.server2.tp3:/srv/nfs_share /srv/nfs
[theo@web1 ~]$ df -hT
Filesystem                      Type      Size  Used Avail [...]
nfs1.server2.tp3:/srv/nfs_share nfs4      6.2G  2.1G  4.2G  34% /srv/nfs
```

* Test

Création d'un dossier depuis web
```
[theo@web1 ~]$ cd /srv/nfs/
[theo@web1 nfs]$ sudo mkdir toto
[theo@nfs1 ~]$ cd /srv/nfs_share/
[theo@nfs1 nfs_share]$ ls
toto
```

Création d'un fichier 

```
[theo@web1 nfs]$ sudo vim tata
[theo@nfs1 nfs_share]$ cat tata
test test
```

# IV. Un peu de théorie : TCP et UDP

* SSH : Protocole TCP
* HTTP : Protocole TCP
* DNS : Protocole UDP
* NFS : Protocole TCP

Fichiers pcap [SSH](./tp3_ssh.pcap) [NFS](./tp3_nfs.pcap) [HTTP](./tp3_http.pcap) [DNS](./tp3_dns.pcap)

* 3-way handshake

Fichier [3_way.pcap](./tp3_3way.pcap)

# V. El final

* Schéma : 

![Schéma](Schéma_Réseau_TP3.drawio.png)

* Tableau des réseaux :
 
| Nom du réseau | Adresse du réseau | Masque          | Nombre de clients possibles | Adresse passerelle | Adresse Broadcast |
|---------------|-------------------|-----------------|-----------------------------|--------------------|-------------------|
| `server1`     | `10.3.50.0`       |`255.255.255.128`| 125                         | `10.3.50.126`      | `10.3.50.127`     |
| `client1`     | `10.3.50.128`     |`255.255.255.192`| 61                          | `10.3.50.190`      | `10.3.50.191`     |
| `server2`     | `10.3.50.192`     |`255.255.255.240`| 13                          | `10.3.50.206`      | `10.3.50.207`     |


* Tableau d'adressage : 

|      Nom machine     | Adresse IP `server1` | Adresse IP `client1` | Adresse IP `server2` | Adresse de passerelle |
|----------------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`         | `10.3.50.126/26`     | `10.3.50.190/26`     | `10.3.50.206/28`     | Carte NAT             |
| `dhcp.client1.tp3`   |  x                   | `10.3.50.189/26`     |  x                   | `10.3.50.190/26`      |
| `marcel.client1.tp3` |  x                   | `10.3.50.131/26`     |  x                   | `10.3.50.190/26`      |
| `dns1.server1.tp3`   | `10.3.50.125/26`     |  x                   |  x                   | `10.3.50.126/26`      |
| `johnny.client1.tp3` |  x                   | `10.3.50.132/26`     |  x                   | `10.3.50.190/26`      |
| `web1.server2.tp3`   |  x                   |  x                   | `10.3.50.205/28`     | `10.3.50.206/28`      |
| `nfs1.server2.tp3`   |  x                   |  x                   | `10.3.50.204/28`     | `10.3.50.206/28`      |


Fichiers de zone [server1.tp3.forward](./server1.tp3.forward) [server2.tp3.forward](./server2.tp3.forward)
Fichier [named.conf](./named.conf)