QUESNOY Théo

# TP4 : Vers un réseau d'entreprise

# Sommaire

- [TP4 : Vers un réseau d'entreprise](#tp4--vers-un-réseau-dentreprise)
- [Sommaire](#sommaire)
- [I. Dumb switch](#i-dumb-switch)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [3. Setup topologie 4](#3-setup-topologie-4)
- [V. Add a building](#v-add-a-building)
  - [3. Setup topologie 5](#3-setup-topologie-5)

# I. Dumb switch

## 3. Setup topologie 1

* Définir IP

VPCS1 : 

```
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0
PC1> show ip
NAME        : PC1[1]
IP/MASK     : 10.1.1.1/24
[...]
PC1> wr
Saving startup configuration to startup.vpc
.  done
```

VPCS2 : 

```
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0
PC2> show ip
NAME        : PC2[1]
IP/MASK     : 10.1.1.2/24
[...]
PC2> wr
Saving startup configuration to startup.vpc
.  done
```

* Ping entre les 2 

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.189 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=2.333 ms
```

# II. VLAN

## 3. Setup topologie 2

* Ajout VPCS3

```
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0
PC3> wr
Saving startup configuration to startup.vpc
.  done
```

* Vérification ping

```
PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=6.230 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=9.073 ms

PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=7.797 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=9.316 ms
```

* Création des VLANs

```
Switch>enable
Switch#conf t
Switch(config)#vlan 10
Switch(config-vlan)#name pc1_2
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#name pc3
Switch(config-vlan)#exit
Switch(config)#exit
Switch#show vlan

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
10   pc1_2                            active
20   pc3                              active
```

* Attribution des VLANs aux ports du switch

```
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#interface GigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit
Switch(config)#exit
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Gi0/1, Gi0/2, Gi0/3, Gi1/0
                                                Gi1/1, Gi1/2, Gi1/3, Gi2/0
                                                Gi2/1, Gi2/2, Gi2/3, Gi3/0
                                                Gi3/1, Gi3/2, Gi3/3
10   pc1_2                            active    Gi0/0, Gi0/1
20   pc3                              active    Gi0/2
```

* Vérification ping VLANs

PC1 et PC2 peuvent se ping : 

```
PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=6.067 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=5.300 ms
```


PC3 ne peut ping ni le PC1 ni le PC2 : 
```
PC3> ping 10.1.1.1
host (10.1.1.1) not reachable
PC3> ping 10.1.1.2
host (10.1.1.2) not reachable
```

# III. Routing

## 3. Setup topologie 3

* Configuration des adresses IP

pc1.clients.tp4 : 

```
pc1.clients> ip 10.1.1.1/24
Checking for duplicate address...
pc1.clients : 10.1.1.1 255.255.255.0
pc1.clients> show ip

NAME        : pc1.clients[1]
IP/MASK     : 10.1.1.1/24
```

pc2.clients.tp4 : 

```
pc2.clients> ip 10.1.1.2/24
Checking for duplicate address...
pc2.clients : 10.1.1.2 255.255.255.0
pc2.clients> show ip

NAME        : pc2.clients[1]
IP/MASK     : 10.1.1.2/24
```

```
adm1.admins.tp4 : 

adm1.admins> ip 10.2.2.1/24
Checking for duplicate address...
adm1.admins : 10.2.2.1 255.255.255.0
adm1.admins> show ip

NAME        : adm1.admins[1]
IP/MASK     : 10.2.2.1/24
```

web1.servers.tp4 : 

```
TYPE=Ethernet
BOOTPROTO=static
DEFROUTE=yes
NAME=enp0s3
DEVICE=enp0s3
ONBOOT=yes
IPADDR=10.3.3.1
NETMASK=255.255.255.0
```

* Création VLANs : 

```
Switch>enable
Switch#conf t
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#exit
```

* Ajout des VLANs aux ports du switch 

```
Switch#conf t
Switch(config)#interface GigabitEthernet0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 11
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 12
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet0/3
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 13
Switch(config-if)#exit
Switch(config)#exit
Switch#
Switch#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
[...]
11   clients                          active    Gi0/0, Gi0/1
12   admins                           active    Gi0/2
13   servers                          active    Gi0/3
[...]
```

* Port vers le routeur en mode trunk 

```
Switch#conf t
Switch(config)#interface GigabitEthernet1/0
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Gi1/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Gi1/0       1-4094

Port        Vlans allowed and active in management domain
Gi1/0       1,11-13

Port        Vlans in spanning tree forwarding state and not pruned
Gi1/0       none
```

* Configuration du routeur

```
R1#conf t
R1(config)#interface fastEthernet0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface fastEthernet0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
R1(config)#interface fastEthernet0/0
R1(config-if)#no shut
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
```

* Vérification ping avec le routeur

```
R1#ping 10.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 12/22/36 ms
R1#ping 10.1.1.2

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.1.1.2, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 8/15/24 ms
R1#ping 10.2.2.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.2.2.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 8/16/32 ms
R1#ping 10.3.3.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 10.3.3.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 12/20/36 ms
```

* Ajout des routes par défaut 

pc1.clients.tp4 : 

```
pc1.clients> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
pc1.clients : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

pc1.clients> show ip

NAME        : pc1.clients[1]
IP/MASK     : 10.1.1.1/24
GATEWAY     : 10.1.1.254
```

pc2.clients.tp4 : 

```
pc2.clients> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
pc2.clients : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

pc2.clients> show ip

NAME        : pc2.clients[1]
IP/MASK     : 10.1.1.2/24
GATEWAY     : 10.1.1.254
```

adm1.admins.tp4 : 

```
adm1.admins> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1.admins : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

adm1.admins> show ip

NAME        : adm1.admins[1]
IP/MASK     : 10.2.2.1/24
GATEWAY     : 10.2.2.254
```

web1.servers.tp4 : 

```
[theo@web1 ~]$ sudo vim /etc/sysconfig/network
[theo@web1 ~]$ cat /etc/sysconfig/network
GATEWAY=10.3.3.254
[theo@web1 ~]$ ip r s
default via 10.3.3.254 dev enp0s3 proto static metric 100
```

* Ping tests
```

pc1.clients> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=59.595 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=39.831 ms
```

```
[theo@web1 ~]$ ping -c 2 10.1.1.2
PING 10.1.1.2 (10.1.1.2) 56(84) bytes of data.
64 bytes from 10.1.1.2: icmp_seq=1 ttl=63 time=39.8 ms
64 bytes from 10.1.1.2: icmp_seq=2 ttl=63 time=27.3 ms 
--- 10.1.1.2 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 27.256/33.545/39.835/6.292 ms
```

```
adm1.admins> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=36.412 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=21.527 ms
```

# IV. NAT

## 3. Setup topologie 4

* Ping vers Cloud1

```
R1#conf t
R1(config)#interface fastEthernet1/0
R1(config-if)#ip address dhcp
R1(config-if)#no shut
R1(config-if)#exit
R1(config)#exit
R1#
*Mar  1 01:31:12.319: %LINK-3-UPDOWN: Interface FastEthernet1/0, changed state to up
*Mar  1 01:31:12.995: %SYS-5-CONFIG_I: Configured from console by console
*Mar  1 01:31:13.319: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up
R1#sh ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
.!!!!
Success rate is 80 percent (4/5), round-trip min/avg/max = 8/28/44 ms
```

* Configuration NAT

```
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface fastEthernet1/0
R1(config-if)#ip nat outside

*Mar  1 01:34:43.903: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface fastEthernet0/0
R1(config-if)#ip nat inside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface fastEthernet1/0 overload
```

* Test ping vers Cloud1 

```
[theo@web1 ~]$ ping -c 2 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=53 time=45.9 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=53 time=42.5 ms
--- 1.1.1.1 ping statistics --- 
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 42.515/44.209/45.903/1.694 ms
```

```
pc1.clients> ping 1.1.1.1

1.1.1.1 icmp_seq=1 timeout
84 bytes from 1.1.1.1 icmp_seq=2 ttl=53 time=42.356 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=53 time=42.284 ms
```

```
adm1.admins> ping 1.1.1.1

1.1.1.1 icmp_seq=1 timeout
84 bytes from 1.1.1.1 icmp_seq=2 ttl=53 time=40.547 ms
84 bytes from 1.1.1.1 icmp_seq=3 ttl=53 time=43.096 ms
```

* Ajout DNS + test 

pc1.clients.tp4 : 

```
pc1.clients> ip dns 8.8.8.8

pc1.clients> ping google.com
google.com resolved to 216.58.213.78

84 bytes from 216.58.213.78 icmp_seq=1 ttl=112 time=39.746 ms
84 bytes from 216.58.213.78 icmp_seq=2 ttl=112 time=41.619 ms
```

adm1.admins.tp4 : 

```
adm1.admins> ip dns 8.8.8.8

adm1.admins> ping google.com
google.com resolved to 216.58.213.78

84 bytes from 216.58.213.78 icmp_seq=1 ttl=112 time=53.817 ms
84 bytes from 216.58.213.78 icmp_seq=2 ttl=112 time=38.125 ms
```

web1.servers.tp4 : 

```
[theo@web1 ~]$ ping -c 2 google.com
PING google.com (216.58.213.78) 56(84) bytes of data.
64 bytes from par21s18-in-f14.1e100.net (216.58.213.78): icmp_seq=1 ttl=112 time=50.8 ms
64 bytes from par21s18-in-f14.1e100.net (216.58.213.78): icmp_seq=2 ttl=112 time=46.2 ms
--- google.com ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time
rtt min/avg/max/mdev = 46.227/48.505/50.783/2.278 ms
[theo@web1 ~]$ cat /etc/resolv.conf
search auvence.co servers.tp4
nameserver 8.8.8.8
```

# V. Add a building

## 3. Setup topologie 5

Fichiers de configs : 

Fichiers [switch1](./Configs/show-running-config_switch1.txt) [switch2](./Configs/show-running-config_switch2.txt) [switch3](./Configs/show-running-config_switch3.txt) [routeur1](./Configs/show-running-config_router1.txt)

* DHCP

```
pc3.clients> dhcp
DDORA IP 10.1.1.55/24 GW 10.1.1.254

pc3.clients> show ip

NAME        : pc3.clients[1]
IP/MASK     : 10.1.1.55/24
GATEWAY     : 10.1.1.254
DNS         : 8.8.8.8
DHCP SERVER : 10.1.1.253
DHCP LEASE  : 593, 600/300/525
DOMAIN NAME : clients.tp4
MAC         : 00:50:79:66:68:05
LPORT       : 20068
RHOST:PORT  : 127.0.0.1:20069
MTU         : 1500
```

* Ping 8.8.8.8

```
pc3.clients> ping 8.8.8.8

*10.1.1.254 icmp_seq=1 ttl=255 time=54.260 ms (ICMP type:3, code:1, Destination host unreachable)
*10.1.1.254 icmp_seq=2 ttl=255 time=63.987 ms (ICMP type:3, code:1, Destination host unreachable)
```

* Ping web1

```
pc3.clients> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=97.945 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=97.243 ms
```

* Ping nom de domaine

```
pc3.clients> ping google.com
google.com resolved to 172.217.19.238

84 bytes from 172.217.19.238 icmp_seq=1 ttl=117 time=92.793 ms
84 bytes from 172.217.19.238 icmp_seq=2 ttl=117 time=133.648 ms
```