QUESNOY Théo

# TP2 : On va router des trucs

## I. ARP

### 1. Echange ARP

Ping de node1 vers node2 : 
```
[theo@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.752 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=1.02 ms
64 bytes from 10.2.1.12: icmp_seq=3 ttl=64 time=0.981 ms
64 bytes from 10.2.1.12: icmp_seq=4 ttl=64 time=1.04 ms
64 bytes from 10.2.1.12: icmp_seq=5 ttl=64 time=0.874 ms
^C
--- 10.2.1.12 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4016ms
rtt min/avg/max/mdev = 0.752/0.932/1.040/0.109 ms
```

Table ARP de node1 : 
```
[theo@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4b REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:ff:50:3b STALE
```
L'adresse MAC de node2 est `08:00:27:ff:50:3b`

Table ARP de node2 : 
```
[theo@node2 ~]$ ip neigh show
10.2.1.11 dev enp0s8 lladdr 08:00:27:ac:f3:24 STALE
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4b REACHABLE
```
L'adresse MAC de node1 est `08:00:27:ac:f3:24`

La table ARP de node1 : 
```
[theo@node1 ~]$ ip neigh show
10.2.1.1 dev enp0s8 lladdr 0a:00:27:00:00:4b REACHABLE
10.2.1.12 dev enp0s8 lladdr 08:00:27:ff:50:3b STALE
```

Cartes réseaux de node2 : 
```
[theo@node2 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e9:35:67 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ff:50:3b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feff:503b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

### 2. Analyse de trames

![image alt](../images/TP2/ARP.PNG)

| ordre | type trame  | source                    | destination                |
|-------|-------------|-------------------------- |----------------------------|
| 1     | Requête ARP |`node2` `08:00:27:ff:50:3b`| Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP |`node1` `08:00:27:ac:f3:24`|`node2` `08:00:27:ff:50:3b` |
| 3     | Requête ARP |`node1` `08:00:27:ac:f3:24`|`node2` `08:00:27:ff:50:3b` |
| 4     | Réponse ARP |`node2` `08:00:27:ff:50:3b`|`node1` `08:00:27:ac:f3:24` |

Après avoir vidé les tables ARP de chaque VMs, la requête ARP a démarré par la trame de broadcast (j'ai ping depuis node2). La réponse ARP est donc node1 qui répond à node2. De ce fait, node2 récupérera l'adresse MAC de node1.
Comme node1 avait également une table ARP vide, il a donc fait les mêmes étapes que node2 à l'exception de la trame de broadcast qu'il n'a pas besoin de diffuser à tout le monde car il connait déjà l'adresse MAC de node2.

## II. Routage

### 1. Mise en place du routage

* Configuration VM Routeur
Nom changé sur la VM routeur : 
```
[theo@routeur ~]$ hostname
routeur.net2.tp2
```
Adresses IP configurées : 
```
[theo@routeur ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:1d:4a:ea brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
       valid_lft 85897sec preferred_lft 85897sec
    inet6 fe80::a00:27ff:fe1d:4aea/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:fa:e6:37 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.254/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fefa:e637/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:13:cf:ad brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.254/24 brd 10.2.2.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe13:cfad/64 scope link
       valid_lft forever preferred_lft forever
```

Activation du routage : 
```
[theo@routeur ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[theo@routeur ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8 enp0s9
[theo@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[theo@routeur ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

```
[theo@routeur ~]$ sudo firewall-cmd --list-all
public (active)
[...]
  masquerade: yes
[...]
```

* Configuration VM marcel

Nom changé : 
```
[theo@marcel ~]$ hostname
marcel.net2.tp2
```
NAT désactivée : 
```
[theo@marcel ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:cb:f5:6a brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:05:17:53 brd ff:ff:ff:ff:ff:ff
    inet 10.2.2.12/24 brd 10.2.2.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe05:1753/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

Ajout de la route statique : 
```
[theo@marcel ~]$ sudo ip route add 10.2.1.0/24 via 10.2.2.254
```

* VM node1

Carte NAT désactivée : 
```
[theo@node1 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:f6:dd:94 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ac:f3:24 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feac:f324/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```
Ajout de la route statique : 
`[theo@node1 ~]$ sudo ip route add 10.2.2.0/24 via 10.2.1.254`

Ping de node1 vers marcel : 
```
[theo@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.900 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=1.77 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=63 time=1.92 ms
64 bytes from 10.2.2.12: icmp_seq=4 ttl=63 time=1.81 ms
^C
--- 10.2.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3020ms
rtt min/avg/max/mdev = 0.900/1.598/1.915/0.409 ms
```

Ping de marcel vers node1 : 
```
[theo@marcel ~]$ ping 10.2.1.11
PING 10.2.1.11 (10.2.1.11) 56(84) bytes of data.
64 bytes from 10.2.1.11: icmp_seq=1 ttl=63 time=0.779 ms
64 bytes from 10.2.1.11: icmp_seq=2 ttl=63 time=1.24 ms
64 bytes from 10.2.1.11: icmp_seq=3 ttl=63 time=1.74 ms
64 bytes from 10.2.1.11: icmp_seq=4 ttl=63 time=1.81 ms
^C
--- 10.2.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3012ms
rtt min/avg/max/mdev = 0.779/1.391/1.807/0.416 ms
```

### 2. Analyse de trames

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         |`node1` `08:00:27:ac:f3:24`| x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         |`router``08:00:27:fa:e6:37`| x              | `node1` `08:00:27:ac:f3:24`|
| 3     | Ping        | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|10.2.2.12       |`router``08:00:27:fa:e6:37` |
| 4     | Requête ARP | x         |`router``08:00:27:13:cf:ad`| x              | Broadcast `FF:FF:FF:FF:FF` |
| 5     | Réponse ARP | x         |`marcel``08:00:27:05:17:53`| x              | `router``08:00:27:13:cf:ad`|
| 6     | Ping        | 10.2.1.254|`router``08:00:27:13:cf:ad`|10.2.2.12       |`marcel``08:00:27:05:17:53` |
| 7     | Pong        | 10.2.2.12 |`marcel``08:00:27:05:17:53`|10.2.2.254      | `router``08:00:27:13:cf:ad`|
| 8     | Pong        | 10.2.1.254|`router``08:00:27:fa:e6:37`|10.2.1.11       | `node1` `08:00:27:ac:f3:24`|

La VM routeur n'a pas besoin d'envoyer une trame de broadcast car il connait déjà l'adresse MAC de la VM node1, tout comme marcel qui n'a pas besoin d'envoyer une trame de broadcast à la VM routeur.

### 3. Accès internet

Ping 8.8.8.8 de la VM marcel : 
```
[theo@marcel ~]$ sudo ip route add default via 10.2.2.254
[theo@marcel ~]$ ip r s
default via 10.2.2.254 dev enp0s8
[...]
[theo@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=87.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=100 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=19.8 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=25.8 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 19.784/58.239/100.356/35.817 ms
```

Ping 8.8.8.8 de la VM node1 : 
```
[theo@node1 ~]$ sudo ip route add default via 10.2.1.254
[theo@node1 ~]$ ip r s
default via 10.2.1.254 dev enp0s8
[...]
[theo@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=19.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=43.9 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=20.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=20.7 ms
^C
--- 8.8.8.8 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3005ms
rtt min/avg/max/mdev = 19.256/26.104/43.949/10.319 ms
```

(DNS via routeur non réussi)

* Analyse de trames

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Ping        | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|8.8.8.8         |`router``08:00:27:fa:e6:37` |
| 2     | Pong        | 8.8.8.8   |`router``08:00:27:fa:e6:37`|10.2.1.11       | `node1` `08:00:27:ac:f3:24`|


## III. DHCP

### 1. Mise en place du serveur DHCP

Installation paquet serveur dhcp : 
`[theo@node1 ~]$ sudo dnf install -y dhcp-server`

Config du fichier dhpcd.conf : 
```
[theo@node1 ~]$ sudo cat /etc/dhcp/dhcpd.conf
#
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page

# create new
# specify domain name
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.200 10.2.1.254;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.1;
}
```

Liste des utilisateurs connectés au serveur DHCP : 
```
[theo@node1 ~]$ cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

lease 10.2.1.200 {
  starts 0 2021/09/25 15:47:52;
  ends 0 2021/09/26 15:47:52;
  cltt 0 2021/09/26 15:47:52;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:ff:50:3b;
  uid "\001\010\000'\377P;";
  client-hostname "node2";
}
```

Autorisation du firewall : 
```
[theo@node1 ~]$ sudo firewall-cmd --add-service=dhcp --permanent
success
```

Fichier enp0s8 de node2 : 
`TYPE=Ethernet                                                                                                           PROXY_METHOD=none                                                                                                       BROWSER_ONLY=no                                                                                                         BOOTPROTO=dhcp                                                                                                          DEFROUTE=yes                                                                                                            IPV4_FAILURE_FATAL=no                                                                                                   IPV6INIT=yes                                                                                                            IPV6_AUTOCONF=yes                                                                                                       IPV6_DEFROUTE=yes                                                                                                       IPV6_FAILURE_FATAL=no                                                                                                   NAME=enp0s8                                                                                                             UUID=ebf8f526-39f4-42c5-9c91-b6fb574a8ede                                                                               DEVICE=enp0s8                                                                                                           ONBOOT=yes `

Adresse IP de node2 : 
```
[theo@node2 ~]$ ip a
[...]
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:e9:35:67 brd ff:ff:ff:ff:ff:ff
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ff:50:3b brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.200/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 490sec preferred_lft 490sec
    inet6 fe80::a00:27ff:feff:503b/64 scope link noprefixroute
       valid_lft forever preferred_lft forever
```

ping vers sa passerelle : 

```
[theo@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.864 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=1.05 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1054ms
rtt min/avg/max/mdev = 0.864/0.954/1.045/0.095 ms
```

ping 8.8.8.8 : 
```
[theo@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=112 time=80.0 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=112 time=84.1 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 80.039/82.076/84.113/2.037 ms
```

Ping de node2 vers marcel : 
```
[theo@node2 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=1.35 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=2.05 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 1.348/1.697/2.046/0.349 ms
```

(DNS non réussi)

### 2. Analyse de trames

![image alt](../images/TP2/DORA+ARP.PNG)

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Discover    | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|10.2.1.200      |`node2``08:00:27:ff:50:3b`  |
| 2     | Offer       | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|10.2.1.200      |`node2``08:00:27:ff:50:3b`  |
| 3     | Request     | 0.0.0.0   |`node2``08:00:27:ff:50:3b` |255.255.255.255 | Broadcast `FF:FF:FF:FF:FF` |
| 4     | ACK         | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|10.2.1.200      |`node2``08:00:27:ff:50:3b`  |
| 5     | ARP Request | 10.2.1.11 |`node1` `08:00:27:ac:f3:24`|10.2.1.200      |`node2``08:00:27:ff:50:3b`  |
| 6     | ARP Reponse | 10.2.1.200|`node2``08:00:27:ff:50:3b` |10.2.1.11       |`node1` `08:00:27:ac:f3:24` |